<?php

namespace CoreDNA;

use CoreDNA\Exceptions\HttpResponseExceptions\HttpErrorException;

/**
 * HttpClient client class for interacting with endpoints.
 */
class HttpClient
{
    /**
     * @var string
     */
    private string $apiEndpoint;

    /**
     * @param string $api_endpoint
     */
    public function __construct(string $api_endpoint)
    {
        $this->apiEndpoint = $api_endpoint;
    }

    /**
     * Get complete url by appending path and query parameters.
     *
     * @param string $path
     * @param array $queryParams
     *
     * @return string
     */
    private function getUrl(string $path = '', array $queryParams = []): string
    {
        $url = $this->apiEndpoint . $path;
        if ( ! empty($queryParams)) {
            $url = $url . '?' . http_build_query($queryParams);
        }

        return $url;
    }

    /**
     * Send request to endpoint.
     *
     * @param string $url
     * @param HttpRequest $request
     *
     * @return HttpResponse
     * @throws \Exception
     */
    private function request(string $url, HttpRequest $request): HttpResponse
    {
        $context  = $request->getContext();
        $result   = @file_get_contents($url, false, $context);
        $response = new HttpResponse($http_response_header, $result);

        if ($response->getStatusCode() >= 400) {
            throw new HttpErrorException($response);
        }

        return $response;
    }

    /**
     * HTTP GET request.
     *
     * @param string $path
     * @param array $headers
     * @param array $queryParams
     *
     * @return HttpResponse
     * @throws \Exception
     */
    public function get(string $path = '', array $headers = [], array $queryParams = []): HttpResponse
    {
        $url      = $this->getUrl($path, $queryParams);
        $request  = new HttpRequest('GET', $headers);
        $response = $this->request($url, $request);

        return $response;
    }

    /**
     * HTTP POST request.
     *
     * @param string $path
     * @param array $headers
     * @param array $data
     *
     * @return HttpResponse
     * @throws \Exception
     */
    public function post(string $path = '', array $headers = [], array $data = []): HttpResponse
    {
        $url      = $this->getUrl($path);
        $request  = new HttpRequest('POST', $headers, $data);
        $response = $this->request($url, $request);

        return $response;
    }

    /**
     * HTTP OPTIONS request.
     *
     * @param string $path
     *
     * @return HttpResponse
     * @throws \Exception
     */
    public function options(string $path = ''): HttpResponse
    {
        $url      = $this->getUrl($path);
        $request  = new HttpRequest('OPTIONS');
        $response = $this->request($url, $request);

        return $response;
    }

}
