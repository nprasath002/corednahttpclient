<?php

namespace CoreDNA;

use CoreDNA\Exceptions\HttpResponseExceptions\JsonSerializationException;
use CoreDNA\Exceptions\HttpResponseExceptions\UnknownContentTypeException;

/**
 * HttpResponse class represents response object.
 */
class HttpResponse
{
    /**
     * @var mixed
     */
    private $rawData;
    /**
     * @var mixed
     */
    private $data;
    /**
     * @var array
     */
    private array $headers;
    /**
     * @var int
     */
    private int $statusCode;
    /**
     * @var string
     */
    private string $contentType;

    /**
     * @param array $headers
     * @param string $body
     *
     * @throws \Exception
     */
    public function __construct(array $headers, string $body)
    {
        $this->headers     = $headers;
        $this->statusCode  = $this->setStatusCode();
        $this->contentType = $this->setContentType();
        $this->rawData     = $body;
        $this->data        = $this->setData($body);
    }

    /**
     * @return string
     */
    private function setContentType()
    {
        $contentType = '';
        foreach ($this->headers as $header) {
            if (preg_match('/^Content-Type:\s+(.+)$/i', $header, $matches)) {
                $contentTypeHeader = explode(';', $matches[1]);
                $contentType       = $contentTypeHeader[0];
                break;
            }
        }

        return $contentType;
    }

    /**
     * Set HTTP status code.
     *
     * @return int
     */
    private function setStatusCode()
    {
        return intval(substr($this->headers[0], 9, 3));
    }

    /**
     * @param string $body
     *
     * @return mixed|string
     * @throws \Exception
     */
    private function setData(string $body)
    {
        $data = '';
        if (empty($body)) {
            return $data;
        }
        switch ($this->contentType) {
            case 'text/plain':
                $data = $body;
                break;
            case 'application/json':
                $data = json_decode($body, true);
                if (JSON_ERROR_NONE !== json_last_error()) {
                    throw new JsonSerializationException($this);
                }
                break;
            default:
                throw new UnknownContentTypeException($this);
        }

        return $data;
    }


    /**
     * Get request headers.
     *
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * Get raw HTTP response.
     *
     * @return mixed|string
     */
    public function getRawData()
    {
        return $this->rawData;
    }

    /**
     * Get response content type.
     *
     * @return string
     */
    public function getContentType(): string
    {
        return $this->contentType;
    }

    /**
     * Get HTTP status code.
     *
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * Get formatted HTTP response.
     *
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }
}
