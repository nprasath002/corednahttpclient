<?php

namespace CoreDNA;

/**
 * HttpRequest class represents request object.
 */
class HttpRequest
{

    /**
     * @var array
     */
    private array $headers;
    /**
     * @var string
     */
    private string $method;
    /**
     * @var array
     */
    private array $data;
    /**
     * @var resource
     */
    private $context;

    /**
     * HttpRequest Constructor.
     *
     * @param string $method
     * @param array $headers
     * @param array $data
     */
    public function __construct(string $method = 'GET', array $headers = [], array $data = [])
    {
        $this->headers = $headers;
        $this->method  = $method;
        $this->data    = $data;
        $this->context = $this->createContext();
    }

    /**
     * Create stream context for http requests.
     *
     * @return resource
     */
    private function createContext()
    {
        if ($this->method === 'POST') {
            $this->headers[] = 'Content-Type: application/json';
        }

        $context_params = [
            'http' => array(
                'method' => $this->method,
                'header' => $this->headers,
            )
        ];

        if ( ! empty($this->data) && $this->method === 'POST') {
            $context_params['http']['content'] = json_encode($this->data);
        }

        return stream_context_create($context_params);
    }

    /**
     * Get stream context of the request.
     *
     * @return resource
     */
    public function getContext()
    {
        return $this->context;
    }
}
