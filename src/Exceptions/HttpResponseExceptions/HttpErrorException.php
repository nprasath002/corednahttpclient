<?php

namespace CoreDNA\Exceptions\HttpResponseExceptions;

use CoreDNA\HttpResponse;

/**
 * HTTP erroneous response exception.
 */
class HttpErrorException extends HttpResponseException
{

    /**
     * HttpErrorException constructor.
     *
     * @param HttpResponse $response
     * @param string $message
     * @param int $code
     * @param \Exception|null $previous
     */
    public function __construct(
        HttpResponse $response,
        string $message = '',
        int $code = 0,
        \Exception $previous = null
    ) {
        if (empty($message)) {
            $message = 'HTTP error ' . $response->getStatusCode();
        }

        parent::__construct($response, $message, $code, $previous);
    }

}
