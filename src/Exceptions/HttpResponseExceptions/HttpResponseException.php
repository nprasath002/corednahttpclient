<?php

namespace CoreDNA\Exceptions\HttpResponseExceptions;

use CoreDNA\HttpResponse;

/**
 * HTTP Response Exceptions
 */
class HttpResponseException extends \Exception
{
    /**
     * @var HttpResponse
     */
    private HttpResponse $response;

    /**
     * @param HttpResponse $response
     * @param string $message
     * @param int $code
     * @param \Exception|null $previous
     */
    public function __construct(
        HttpResponse $response,
        string $message = '',
        int $code = 0,
        \Exception $previous = null
    ) {
        $this->response = $response;

        parent::__construct($message, $code, $previous);
    }

    /**
     * Get HTTP Respsonse object.
     *
     * @return HttpResponse
     */
    public function getResponse(): HttpResponse
    {
        return $this->response;
    }
}
