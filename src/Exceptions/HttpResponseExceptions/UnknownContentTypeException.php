<?php

namespace CoreDNA\Exceptions\HttpResponseExceptions;

use CoreDNA\HttpResponse;

/**
 * Unknown content type exception.
 */
class UnknownContentTypeException extends HttpResponseException
{

    /**
     * @param HttpResponse $response
     * @param string $message
     * @param int $code
     * @param \Exception|null $previous
     */
    public function __construct(
        HttpResponse $response,
        string $message = '',
        int $code = 0,
        \Exception $previous = null
    ) {
        if (empty($message)) {
            $message = 'Unknown content type: ' . $response->getContentType();
        }

        parent::__construct($response, $message, $code, $previous);
    }

}
