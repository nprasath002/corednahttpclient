<?php

namespace CoreDNA\Exceptions\HttpResponseExceptions;

use CoreDNA\HttpResponse;

/**
 * JSON serialization error exception.
 */
class JsonSerializationException extends HttpResponseException
{

    /**
     * @param HttpResponse $response
     * @param string $message
     * @param int $code
     * @param \Exception|null $previous
     */
    public function __construct(
        HttpResponse $response,
        string $message = '',
        int $code = 0,
        \Exception $previous = null
    ) {
        if (empty($message)) {
            $message = 'JSON serialisation error: ' . json_last_error_msg();
        }

        parent::__construct($response, $message, $code, $previous);
    }

}
