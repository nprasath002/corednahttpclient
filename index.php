<?php

require __DIR__ . '/vendor/autoload.php';

$http_client = new CoreDNA\HttpClient(API_ENDPOINT);
try {
    $response = $http_client->options();
    $token    = $response->getData();
    try {
        $headers = [
            'Authorization: Bearer ' . $token
        ];
        $data     = [
            'name'  => 'Prasath Nadarajah',
            'email' => 'n.prasath.002@gmail.com',
            'url'   => 'https://gitlab.com/nprasath002/corednahttpclient'
        ];
        $response = $http_client->post('', $headers, $data);
    } catch (Exception $e) {
        echo $e->getMessage() . PHP_EOL;
    }
} catch (Exception $e) {
    echo $e->getMessage() . PHP_EOL;
}
